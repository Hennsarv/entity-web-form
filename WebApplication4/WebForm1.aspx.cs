﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Entity;

namespace WebApplication4
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        NorthwindEntities ne = new NorthwindEntities();
        

        protected void Page_Load(object sender, EventArgs e)
        {
            
            this.LinqDataSource1.Where = "CategoryID == 0";
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            
            ne.Categories.Add(new Category() { CategoryName = this.TextBox1.Text });
            ne.SaveChanges();
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {

            this.Vastus.Text = GridView1.SelectedDataKey.Value.ToString();
            this.LinqDataSource1.Where = $"CategoryID == {GridView1.SelectedDataKey.Value}";

            

        }
    }
}
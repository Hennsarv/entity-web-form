﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplication4.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Meie kategooriad</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="Nimetus" runat="server" Text="NewCategoyName" CssClass="Ohoo"></asp:Label>
            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
            <asp:Button OnClick="Button1_Click" ID="Button1" runat="server" Text="Lisa uus kategooria" />
            <br />
            <asp:Label ID="Vastus" runat="server"></asp:Label>
            <br />
            <asp:EntityDataSource ID="EntityDataSource1" runat="server" ConnectionString="name=NorthwindEntities" DefaultContainerName="NorthwindEntities" EnableDelete="True" EnableFlattening="False" EnableInsert="True" EnableUpdate="True" EntitySetName="Categories">
            </asp:EntityDataSource>
            <asp:EntityDataSource ID="EntityDataSource2" runat="server" ConnectionString="name=NorthwindEntities" DefaultContainerName="NorthwindEntities" EnableFlattening="False" EntitySetName="Products" Select="it.[ProductID], it.[ProductName], it.[UnitPrice], it.[UnitsInStock]">
            </asp:EntityDataSource>
            <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="WebApplication4.NorthwindEntities" EntityTypeName="" TableName="Products" Select="new (ProductID, ProductName, UnitPrice, UnitsInStock)">
            </asp:LinqDataSource>
            <br />
        </div>
        <table style="vertical-align:top">
            <tr>
                <td>
        <asp:GridView ID="GridView1" runat="server" OnSelectedIndexChanged="GridView1_SelectedIndexChanged" AllowPaging="True" PageSize="20" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="CategoryID" DataSourceID="EntityDataSource1">
            <Columns>
                <asp:BoundField DataField="CategoryID" HeaderText="CategoryID" ReadOnly="True" SortExpression="CategoryID" />
                <asp:BoundField DataField="CategoryName" HeaderText="CategoryName" SortExpression="CategoryName" />
                <asp:BoundField DataField="Description" HeaderText="Description" />
                <asp:CommandField ShowDeleteButton="False" ShowEditButton="True" ShowSelectButton="True" SelectText="Tooted" EditText="Muuda" UpdateText="Salvesta" CancelText="Tühista" />
            </Columns>
        </asp:GridView>

                </td>
                <td>
        <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" DataSourceId="LinqDataSource1" AllowSorting="True">
            <Columns>
                <asp:BoundField DataField="ProductID" HeaderText="ProductID" ReadOnly="True" SortExpression="ProductID" />
                <asp:BoundField DataField="ProductName" HeaderText="ProductName" ReadOnly="True" SortExpression="ProductName" />
                <asp:BoundField DataField="UnitPrice" HeaderText="UnitPrice" ReadOnly="True" SortExpression="UnitPrice" />
                <asp:BoundField DataField="UnitsInStock" HeaderText="UnitsInStock" ReadOnly="True" SortExpression="UnitsInStock" />
            </Columns>
        </asp:GridView>

                </td>
            </tr>
        </table>
    </form>
</body>
</html>
